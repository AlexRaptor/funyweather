//
//  CurrentWeather.swift
//  FunyWeather
//
//  Created by Александр Селиванов on 02/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation
import UIKit

struct CurrentWeather {
    let temperature: Double
    let apparentTemperature: Double
    let humidity: Double
    let pressure: Double
    let icon: UIImage
}

extension CurrentWeather: JSONDecodable {
    init?(JSON: [String: AnyObject]) {
        guard let temperature = JSON["temperature"] as? Double,
            let apparentTemperature = JSON["apparentTemperature"] as? Double,
            let humidity = JSON["humidity"] as? Double,
            let pressure = JSON["pressure"] as? Double,
            let iconString = JSON["icon"] as? String else {
            return nil
        }

        self.temperature = temperature
        self.apparentTemperature = apparentTemperature
        self.humidity = humidity * 100 // приходит значение от 0 до 1
        self.pressure = pressure * 0.750062 // переводим Гекто-Паскали а мм ртутного столба
        icon = WeatherIconManager(rawValue: iconString).image
    }
}

extension CurrentWeather {
    var pressureString: String {
        return "\(Int(pressure)) mm"
    }

    var humidityLabelString: String {
        return "\(Int(humidity))%"
    }

    var temperatureString: String {
        return "\(Int(temperature))˚C"
    }

    var appearentTemperatureString: String {
        return "Feels like \(Int(apparentTemperature))˚C"
    }
}
