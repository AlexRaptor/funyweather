//
//  ViewController.swift
//  FunyWeather
//
//  Created by Александр Селиванов on 02/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var pressureLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var appearentTemperatureLabel: UILabel!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var weatherManager = APIWeatherManager(apiKey: "adb0bd1285fde4ef7fbfde518bf1ffc8")
    let coordinates = Coordinates(latitude: 54.749686, longitude: 83.066545)

    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
       
        fetchCurrentWeatherData()
    }
    
    // MARK: - Location Manager Delegate Methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last! as CLLocation
        if userLocation.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            print("My location - latitude: \(userLocation.coordinate.latitude), logitude:\(userLocation.coordinate.longitude)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationLabel.text = "Location Unavailable"
        
        print(error)
    }
    
    // MARK: - Update Data Methods
    
    func fetchCurrentWeatherData() {
        weatherManager.fetchCurrentWeatherWith(coordinates: coordinates) { result in
            self.toggleActivetyIndicator(on: false)
            switch result {
            case let .Success(currentWeather):
                self.updateUIWith(currentWeater: currentWeather)
            case let .Failure(error as NSError):
                self.showErrorAlert(title: "Unable to get data", message: error.localizedDescription)
            }
        }
    }

    // MARK: - Update UI Methods
    
    func toggleActivetyIndicator(on: Bool) {
        refreshButton.isHidden = on
        
        if on {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func showErrorAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)

        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }

    func updateUIWith(currentWeater: CurrentWeather) {
        imageView.image = currentWeater.icon
        pressureLabel.text = currentWeater.pressureString
        humidityLabel.text = currentWeater.humidityLabelString
        temperatureLabel.text = currentWeater.temperatureString
        appearentTemperatureLabel.text = currentWeater.appearentTemperatureString
    }
    
    // MARK: - Actions

    @IBAction func refreshButtonTapped(_ sender: UIButton) {
        toggleActivetyIndicator(on: true)
        fetchCurrentWeatherData()
    }
}
