//
//  ErrorManager.swift
//  FunyWeather
//
//  Created by Александр Селиванов on 04/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

public let RPTNetworkingErrorDomain = "selivanov.alexander.FunyWeather.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
