//
//  APIWeatherManager.swift
//  FunyWeather
//
//  Created by Александр Селиванов on 04/01/2019.
//  Copyright © 2019 Alexander Selivanov. All rights reserved.
//

import Foundation

struct Coordinates {
    let latitude: Double
    let longitude: Double
}

enum ForecastType: FinalURLPoint {
    case Current(apiKey: String, coordinates: Coordinates)

    var baseURL: URL {
        return URL(string: "https://api.darksky.net")!
    }

    var path: String {
        switch self {
        case let .Current(apiKey, coordinates):
            return "/forecast/\(apiKey)/\(coordinates.latitude),\(coordinates.longitude)?units=si"
        }
    }

    var request: URLRequest {
        let url = URL(string: path, relativeTo: baseURL)
        return URLRequest(url: url!)
    }
}

final class APIWeatherManager: APIManager {
    let sessionConfiguration: URLSessionConfiguration

    lazy var session: URLSession = {
        URLSession(configuration: self.sessionConfiguration)
    }()

    let apiKey: String

    init(sessionConfiguration: URLSessionConfiguration, apiKey: String) {
        self.sessionConfiguration = sessionConfiguration
        self.apiKey = apiKey
    }

    convenience init(apiKey: String) {
        self.init(sessionConfiguration: URLSessionConfiguration.default, apiKey: apiKey)
    }

    func fetchCurrentWeatherWith(coordinates: Coordinates, completionHandler: @escaping (APIResult<CurrentWeather>) -> Void) {
        let request = ForecastType.Current(apiKey: apiKey, coordinates: coordinates).request

        fetch(request: request, parse: { (json) -> CurrentWeather? in
            if let dictionary = json["currently"] as? [String: AnyObject] {
                return CurrentWeather(JSON: dictionary)
            } else {
                return nil
            }
        }, completionHandler: completionHandler)
    }
}
